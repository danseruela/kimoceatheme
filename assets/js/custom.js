( function( $ ) {
// Customer header menu
    $(document).ready(function(){
        $(".header-titles .menu-toggle").click(function(){
            $(".overlay").slideToggle('fast', 'swing');
        $(this).toggleClass('dashicons dashicons-menu').toggleClass('dashicons dashicons-no');
        });

        // Add target attribute to footer social media links
        $("#menu-social-media-menu a, .wp-social-link a").attr("target", "_blank");
        $("#menu-footer-menu li a").each(function(){
            if ($(this).attr("href") === "https://www.facebook.com/CEACreatives") {
                $(this).attr("target", "_blank");
            }
        });

        $(".sfsi_shortcode_container").css("float", "none");
    });
    $('.overlay').on('click', function(){
        $(".overlay").slideToggle('fast', 'swing');   
        $(".header-titles .menu-toggle").toggleClass('dashicons dashicons-menu').toggleClass('dashicons dashicons-no');
        open = false;
    });
}( jQuery ) );