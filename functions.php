<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

// END ENQUEUE PARENT ACTION

// CUSTOM SCRIPT
function kimocea_custom_theme_init() {
    wp_enqueue_script( 'jquery', get_theme_file_uri( '/assets/js/jquery.js' ), false );
    wp_enqueue_script( 'kimocea-custom-js', get_theme_file_uri( '/assets/js/custom.js' ), false );

}

add_action( 'wp_enqueue_scripts', 'kimocea_custom_theme_init' );

// Load dashicons to all users
function ww_load_dashicons(){
    wp_enqueue_style('dashicons');
}
add_action('wp_enqueue_scripts', 'ww_load_dashicons');

// Automatically linked featured images to posts
function wpb_autolink_featured_images( $html, $post_id, $post_image_id ) {
    $html = '<a href="' . get_permalink( $post_id ) . '" title="' . esc_attr( get_the_title( $post_id ) ) . '">' . $html . '</a>';
    return $html;
}
add_filter( 'post_thumbnail_html', 'wpb_autolink_featured_images', 10, 3 );

// Create Read More link using the_excerpt()
function new_excerpt_more($more) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more', 21 );

function the_excerpt_more_link( $excerpt ){
    $post = get_post();
    $excerpt .= '<div class="mobile-button-wrapper has-text-align-center">
                    <a href="'. get_permalink($post->ID) . '"><button class="mobile-button">read more</button></a>
                </div>';
    return $excerpt;
}
add_filter( 'the_excerpt', 'the_excerpt_more_link', 21 );

// Remove Category prefix on archive page.
add_filter( 'get_the_archive_title', function ( $title ) {
    if( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;

});

// Custom Admin Login Logo
function kimocea_login_logo() { 
    echo '<style type="text/css">
            body.login div#login h1 a {
            background-image: url(' . get_stylesheet_directory_uri() . '/assets/kimoblog-white-01-icon.png); 
            background-size: 100px;
            width: 100px;
            height: 100px;
            padding-bottom: 10px; 
          } 
          </style>';
} 
add_action( 'login_enqueue_scripts', 'kimocea_login_logo' );

function kimocea_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'kimocea_login_logo_url' );

// Limit display number of characters in the_content()
// add_filter("the_content", "plugin_myContentFilter");

// function plugin_myContentFilter($content)
// {
//     // Take the existing content and return a subset of it
//     return substr($content, 0, 50);
// }