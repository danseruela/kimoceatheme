<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<main id="site-content" role="main">

	<?php

	$archive_title    = '';
	$archive_subtitle = '';

	if ( is_search() ) {
		global $wp_query;

		$archive_title = sprintf(
			'%1$s %2$s',
			'<span class="color-accent">' . __( 'Search:', 'twentytwenty' ) . '</span>',
			'&ldquo;' . get_search_query() . '&rdquo;'
		);

		if ( $wp_query->found_posts ) {
			$archive_subtitle = sprintf(
				/* translators: %s: Number of search results. */
				_n(
					'We found %s result for your search.',
					'We found %s results for your search.',
					$wp_query->found_posts,
					'twentytwenty'
				),
				number_format_i18n( $wp_query->found_posts )
			);
		} else {
			$archive_subtitle = __( 'We could not find any results for your search. You can give it another try through the search form below.', 'twentytwenty' );
		}
	} elseif ( ! is_home() ) {
		$archive_title    = get_the_archive_title();
		$archive_subtitle = get_the_archive_description();
	}

	if ( $archive_title || $archive_subtitle ) {
		?>

		<header class="archive-header has-text-align-center header-footer-group">

			<div class="archive-header-inner section-inner medium">

				<?php if ( $archive_title ) { ?>
					<h1 class="archive-title"><?php echo wp_kses_post( $archive_title ); ?></h1>
				<?php } ?>

				<?php if ( $archive_subtitle ) { ?>
					<div class="archive-subtitle section-inner thin max-percentage intro-text"><?php echo wp_kses_post( wpautop( $archive_subtitle ) ); ?></div>
				<?php } ?>

			</div><!-- .archive-header-inner -->

		</header><!-- .archive-header -->

	<?php
	}
	
	if ( have_posts() ) {
		if ( is_home() ) {
			// Diplay FEATURED posts limit 4
			$x = 0;
			$featured = new WP_Query( 'cat=8&posts_per_page=4' );
			
			echo '<div id="featured-posts">';

			while($featured->have_posts()) : $featured->the_post(); 
				$x++;
				if ( $x > 1 ) {
					echo '<hr class="post-separator is-style-wide section-inner" aria-hidden="true" />';
				}
				
				get_template_part( 'template-parts/content', get_post_type() );
				
			endwhile;
			wp_reset_postdata();
			echo '</div>';

			
			// Display RECENT/LATEST stories limit 15
			echo '<div id="latest_stories">';	
		
			echo '<div class="content-list-header section-inner">';
			echo '<h3 class="heading-size-3">LATEST STORIES</h3>';
			echo '</div>';

			$y = 0;
			$latest_stories = new WP_Query( 'posts_per_page=15' );

			while ( $latest_stories->have_posts() ) : $latest_stories->the_post();
				$y++;
				if ( $y > 1 ) {
					echo '<hr class="post-separator is-style-wide section-inner" aria-hidden="true" />';
				}

				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;
			wp_reset_postdata();
			echo '</div>';
		} else {
			$i = 0;
			while ( have_posts() ) {
				$i++;
				if ( $i > 1 ) {
					echo '<hr class="post-separator is-style-wide section-inner" aria-hidden="true" />';
				}
				the_post();

				get_template_part( 'template-parts/content', get_post_type() );

			}
		}
	} elseif ( is_search() ) {
		?>

		<div class="no-search-results-form section-inner thin">

			<?php
			get_search_form(
				array(
					'label' => __( 'search again', 'twentytwenty' ),
				)
			);
			?>

		</div><!-- .no-search-results -->

		<?php
	}
	
	?>

	<?php  
		if ( is_search() ) {
			get_template_part( 'template-parts/pagination' ); 
		}
	?>
	<div class="pagination-separator"></div>
</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php
get_footer();
