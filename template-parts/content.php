<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<div class="section-inner">
	<?php

	if ( ! is_search() && ! is_page() ) {
		get_template_part( 'template-parts/featured-image' );
	}
	?>
	<div class="entry-content-wrapper">
	<?php
	get_template_part( 'template-parts/entry-header' );
	?>

	<div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">

		<div class="entry-content">
			<?php		
			if ( is_search() || ! is_singular() && 'summary' === get_theme_mod( 'blog_content', 'full' ) ) {
				// Post meta data.	
				echo '<span class="post-meta">';
				the_time('j M Y');
				echo ' by ';
				!empty(the_author_meta('first_name')) ? the_author_meta('first_name') : '';
				echo ' ';
				!empty(the_author_meta('last_name')) ? the_author_meta('last_name') : '';
				echo '</span>';
				the_excerpt();
			} else {
				// the_content( __( 'Continue reading', 'twentytwenty' ) );
				if ( is_singular('post') || is_page() ) {
					the_content();
				} else {
					the_excerpt();
				}
			}
				//the_excerpt();
			?>
		</div><!-- .entry-content -->

	</div><!-- .post-inner -->
	</div><!-- .entry-content-wrapper -->
	

	<?php

	if ( is_single() ) {

		get_template_part( 'template-parts/navigation' );

	}

	/**
	 *  Output comments wrapper if it's a post, or if comments are open,
	 * or if there's a comment number – and check for password.
	 * */
	// Remove comments on single page.
	// if ( ( is_single() || is_page() ) && ( comments_open() || get_comments_number() ) && ! post_password_required() ) {
		?>

		<!-- <div class="comments-wrapper section-inner"> -->

			<?php //comments_template(); ?>

		<!-- </div>.comments-wrapper -->

		<?php
	// }
	?>
	</div>
</article><!-- .post -->
