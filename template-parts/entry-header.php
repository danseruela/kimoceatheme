<?php
/**
 * Displays the post header
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

$entry_header_classes = '';

if ( is_singular() ) {
	$entry_header_classes .= ' header-footer-group';
}

?>

<header class="entry-header <?php echo esc_attr( $entry_header_classes ); ?>">

	<div class="entry-header-inner medium">

		<?php
			/**
			 * Allow child themes and plugins to filter the display of the categories in the entry header.
			 *
			 * @since Twenty Twenty 1.0
			 *
			 * @param bool   Whether to show the categories in header, Default true.
			 */
		/********************************
		 * REMOVE CATEGORIES, AUTHOR, POST DETAILS, COMMENTS	
		 *********************************/ 
		//$show_categories = apply_filters( 'twentytwenty_show_categories_in_entry_header', true );

		//if ( true === $show_categories && has_category() ) {
			?>

			<!-- <div class="entry-categories">
				<span class="screen-reader-text"><?php // _e( 'Categories', 'twentytwenty' ); ?></span> -->
				<!-- <div class="entry-categories-inner"> -->
					<?php // the_category( ' ' ); ?>
				<!-- </div>.entry-categories-inner -->
			<!-- </div>.entry-categories -->

			<?php
		// }

		if ( is_singular() ) {
			if ( is_page('About') || is_page('Keep In Touch') ) {
				the_title( '<h1 class="entry-title center-align">', '</h1>' );
				edit_post_link();
			} else {
				the_title( '<h1 class="entry-title">', '</h1>' );
			}
		} else {
			the_title( '<h2 class="entry-title heading-size-1"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
		}

		$intro_text_width = '';

		if ( is_singular() ) {
			$intro_text_width = ' small';
		} else {
			$intro_text_width = ' thin';
		}

		// if ( has_excerpt() && is_singular() ) {
			?>

			<!-- <div class="intro-text max-percentage<?php echo $intro_text_width; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- static output ?>"> -->
				<?php // the_excerpt(); ?>
			<!-- </div> -->

			<?php
		// }
		
		if ( is_singular('post') ) {
			echo '<div class="post-meta-wrapper-custom">';
			echo '<span class="author-custom">Written by ';
			!empty(the_author_meta('first_name')) ? the_author_meta('first_name') : '';
			echo ' ';
			!empty(the_author_meta('last_name')) ? the_author_meta('last_name') : '';
			echo "</span>";
			echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]');
			edit_post_link();
			echo '<hr>';

			// Display post categories
			echo '<div class="category-custom">';
			the_category(' | ');
			echo '</div>';
			echo "</div>";
		}

		// Default to displaying the post meta.
		// twentytwenty_the_post_meta( get_the_ID(), 'single-top' );
		?>

	</div><!-- .entry-header-inner -->

</header><!-- .entry-header -->
